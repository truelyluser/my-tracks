<!--
 * @Author: 一根鱼骨棒 Email 775639471@qq.com
 * @Date: 2023-11-27 11:56:52
 * @LastEditTime: 2023-11-27 12:01:17
 * @LastEditors: 一根鱼骨棒
 * @Description: 本开源代码使用GPL 3.0协议
 * Software: VScode
 * Copyright 2023 迷舍
-->
# 出行轨迹

----

简单记录个人出行轨迹的网页程序，开发难度低，上手简单。

----
技术栈：
Layui + Python + SQLite3
Python依赖：Flask  

## 如何使用

1、建立SQLite3数据库，导入main.sql建立数据库结构
2、运行app.py 即可访问网页  
