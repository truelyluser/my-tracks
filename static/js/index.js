var laydate = layui.laydate;
var $ = layui.$;
var form = layui.form;
var routes = [];
// 日期
laydate.render({
  elem: "#date",
  fullPanel: true,
  type: "datetime",
});
// 渲染表格
$.ajax({
  url: "http://localhost/getCityList",
  data: "",
  async: true,
  dataType: "json",
  success: function (array) {
    cities = '<option value="">直接选择或搜索选择</option>';
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      cities += '<option value="' + array[index] + '">' + array[index] + "</option>";
    }
    $(".start").html(cities);
    $(".end").html(cities);
    form.render($("#route-form"));
  },
});
var uploadedDataURL = "/static/js/china.json";
var myChart = echarts.init(document.getElementById("map"));
myChart.showLoading();
var option = {
  backgroundColor: "transparent",
  // title: {
  //   top: 20,
  //   text: "“困难人数” - 杭州市",
  //   subtext: "",
  //   x: "center",
  //   textStyle: {
  //     color: "#ccc",
  //   },
  // },

  // tooltip: {
  //   trigger: "item",
  //   formatter: function (params) {
  //     console.log(params);
  //     return params.name + " : " + params.value[2];
  //   },
  // },
  // visualMap: {
  //   min: 0,
  //   max: 1000000,
  //   right: 100,
  //   seriesIndex: 1,
  //   type: "piecewise",
  //   bottom: 100,
  //   textStyle: {
  //     color: "#FFFF",
  //   },
  //   splitList: [
  //     {
  //       gt: 50000,
  //       color: "#F5222D",
  //       label: "困难人数大于5万人",
  //     }, //大于5万人
  //     {
  //       gte: 30000,
  //       lte: 50000,
  //       color: "#FA541C ",
  //       label: "困难人数3-5万人",
  //     }, //3-5万人
  //     {
  //       gte: 10000,
  //       lte: 30000,
  //       color: "#FA8C16",
  //       label: "困难人数1-3万人",
  //     }, //1-3万人
  //     {
  //       lte: 10000,
  //       color: "#fbe1d6",
  //       label: "困难人数小于1万人",
  //     },
  //   ],
  // },
  // 地图属性配置
  geo: {
    map: "china",
    aspectScale: 0.8, //长宽比
    zoom: 1.2,
    roam: false,
    label: {
      //设置字体样式
      show: true, //字体是否显示
      fontSize: 14, //设置字体大小
      color: "#fff", //设置字体颜色
    },
    itemStyle: {
      normal: {
        borderColor: "rgba(255,209,163, .5)",
        borderWidth: 1,
        areaColor: "rgba(73,86,166,.1)",
        borderWidth: 0.5, //区域边框宽度
        shadowBlur: 5,
        shadowColor: "rgba(107,91,237,.7)",
      },
      emphasis: {
        borderColor: "#FFD1A3",
        areaColor: "rgba(102,105,240,.3)",
        borderWidth: 0.5,
        shadowBlur: 5,
        shadowColor: "rgba(135,138,255,.5)",
        borderWidth: 1,
        label: {
          show: true,
        },
      },
    },
    // regions: [
    //   {
    //     name: "南海诸岛",
    //     itemStyle: {
    //       areaColor: "rgba(0, 10, 52, 1)",

    //       borderColor: "rgba(0, 10, 52, 1)",
    //       normal: {
    //         opacity: 0,
    //         label: {
    //           show: true,
    //           color: "#009cc9",
    //         },
    //       },
    //     },
    //   },
    // ],
  },
  series: [
    {
      type: "effectScatter",
      coordinateSystem: "geo", //该系列使用的坐标系
      showEffectOn: "render",
      zlevel: 1,
      rippleEffect: {
        //特效相关配置
        period: 15,
        scale: 4,
        brushType: "fill",
      },
      hoverAnimation: true,
      label: {
        normal: {
          formatter: "{b}",
          position: "right",
          offset: [1, 0],
          color: "#F58158",
          show: true,
        },
      },
      itemStyle: {
        normal: {
          color: "#46bee9",
          shadowBlur: 10,
          shadowColor: "#333",
        },
      },
      symbolSize: 5,
      data: points,
    }, //地图线的动画效果
    // 飞机 series 1
    {
      type: "lines",
      zlevel: 2,
      effect: {
        show: true,
        period: 8, //箭头指向速度，值越小速度越快
        constantSpeed: 30,
        trailLength: 0.2, //特效尾迹长度[0,1]值越大，尾迹越长重
        // 飞机
        symbol:
          "path://M599.06048 831.309824l12.106752-193.404928 372.860928 184.430592L984.02816 710.206464 617.906176 367.33952 617.906176 151.638016c0-56.974336-46.188544-143.064064-103.158784-143.064064-56.974336 0-103.158784 86.089728-103.158784 143.064064L411.588608 367.33952 45.461504 710.206464l0 112.129024 366.660608-184.430592 14.999552 209.27488c0 5.05344 0.594944 9.892864 1.124352 14.749696l-66.591744 60.348416 0 66.587648 153.986048-50.879488 2.43712-0.80896 147.439616 51.688448 0-66.587648-68.758528-62.253056L599.06048 831.309824z ", //箭头图标
        symbolSize: 12, //图标大小
      },
      lineStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(
            0,
            0,
            0,
            1,
            [
              {
                offset: 0,
                color: "#58B3CC",
              },
              {
                offset: 1,
                color: "#F58158",
              },
            ],
            false
          ),
          width: 1, //线条宽度
          opacity: 0.1, //尾迹线条透明度
          curveness: 0.1, //尾迹线条曲直度
        },
      },
      // 飞机轨迹数据
      data: [],
    },
    // 火车  series 2
    {
      type: "lines",
      polyline: true,
      zlevel: 2,
      effect: {
        show: true,
        period: 16, //箭头指向速度，值越小速度越快
        trailLength: 0.3, //特效尾迹长度[0,1]值越大，尾迹越长重
        // 火车
        symbol:
          "path://M304.244973 732.218161a43.350032 43.350032 0 0 0 59.606293-14.33441 43.350032 43.350032 0 0 0-14.341635-59.606294C257.549764 601.987441 202.654173 504.110294 202.654173 396.457715c0-169.115699 137.585776-306.701475 306.701475-306.701474 169.115699 0 306.701475 137.585776 306.701475 306.701474 0 104.610852-52.547463 200.999647-140.562478 257.845989a43.342807 43.342807 0 0 0-12.896635 59.931419 43.328357 43.328357 0 0 0 59.931419 12.896635c112.854583-72.878628 180.227757-196.491244 180.227757-330.674043 0-216.916334-176.477979-393.401538-393.401538-393.401538-216.916334 0-393.401538 176.477979-393.401538 393.401538-0.007225 138.069851 70.386002 263.589868 188.290863 335.760446z,M869.088662 935.363635H552.698455V300.73362a43.350032 43.350032 0 0 0-86.700064 0v634.630015H149.60096a43.350032 43.350032 0 0 0 0 86.700064h719.487702a43.350032 43.350032 0 0 0 0-86.700064z", //箭头图标
        symbolSize: 8, //图标大小
      },
      lineStyle: {
        normal: {
          color: "#fff",
          width: 2, //线条宽度
          opacity: 0.1, //尾迹线条透明度
          curveness: 0, //尾迹线条曲直度
          type: "dashed",
        },
      },
      // 火车轨迹数据
      data: [],
    },
    // 自驾  series 3
    {
      type: "lines",
      polyline: true,
      zlevel: 2,
      effect: {
        show: true,
        period: 16, //箭头指向速度，值越小速度越快
        trailLength: 0.3, //特效尾迹长度[0,1]值越大，尾迹越长重
        // 汽车
        symbol:"path://M201.4 349.4l9.8-9.2V330c0-11.3 9.2-20.5 20.5-20.5h18.9C263.9 307 439 271.1 515.3 288c74.8 16.9 193.5 94.7 205.3 102.4l126 33.8c39.9 10.8 63.5 46.1 63.5 94.2v62.5c0 19.5-15.9 35.3-35.3 35.3h-63.5c-3.1 0-5.6-2.6-5.6-5.6 0-3.1 2.6-5.6 5.6-5.6h63.5c13.3 0 24.1-10.8 24.1-24.1v-62.5c0-43-20.5-74.2-55.8-83.5l-127-34.3c-0.5 0-1 0-1.5-0.5-1-0.5-127-84-202.2-100.9-75.3-16.4-258.6 21.5-260.6 22h-20.5c-5.1 0-9.2 4.1-9.2 9.2v12.8c0 1.5-0.5 3.1-1.5 4.1l-12 11.1M183 382l-4.9 4.6-12 11.1c-1 1-2 1.5-3.6 1.5-12.8 0-23 10.2-23 23v62c0 3.1-2.6 5.6-5.6 5.6-5.1 0-9.2 4.1-9.2 9.2v81.9c0 13.3 10.8 24.1 24.1 24.1h44.5c3.1 0 5.6 2.6 5.6 5.6 0 3.1-2.6 5.6-5.6 5.6h-44.5c-19.5 0-35.3-15.9-34.8-34.8v-81.9c0-9.7 6.1-17.4 14.8-20v-57.3c0-18.4 13.8-33.3 31.7-34.3l11.4-10.7 4.6-4.3m-27.8 243.3,M116.3 612.5c-8.3-8.5-12.7-19.6-12.4-31.3v-81.8c0-11.1 5.7-21 14.8-26.4v-50.9c0-22.2 16.1-40.8 37.4-43.9l13.5-12.7c4-3.8 10.4-3.6 14.1 0.4 1.7 1.9 2.6 4.2 2.7 6.6 1.4 0.5 2.8 1.4 3.9 2.6 3.7 4.1 3.5 10.4-0.6 14.1l-16.8 15.5c-2.9 2.9-6.5 4.3-10.5 4.3-7.3 0-13 5.7-13 13v62c0 8.4-6.6 15.2-14.8 15.6v81.2c0 7.8 6.3 14.1 14.1 14.1h44.5c8.6 0 15.6 7 15.6 15.6s-7 15.6-15.6 15.6h-44.5c-12.1 0.1-23.9-4.9-32.4-13.6z m679.3-1.9c0-8.6 7-15.6 15.6-15.6h63.5c7.8 0 14.1-6.3 14.1-14.1v-62.5c0-38.9-17.6-65.8-48.4-73.8l-126.1-34.1c-2.2-0.3-4.1-1-5.7-2.3-0.3-0.2-0.7-0.5-1.2-0.8-30.2-19.4-133.6-84.1-197.3-98.4-73-15.9-253.4 21.4-256.2 22-0.7 0.2-1.5 0.3-2.3 0.3H232v12c0 4.3-1.6 8.3-4.5 11.2l-0.3 0.3-12 11.1c-4.1 3.7-10.4 3.5-14.1-0.6-1.6-1.8-2.5-4-2.6-6.3-1.6-0.5-3.1-1.4-4.3-2.7-3.8-4-3.6-10.4 0.4-14.1l6.6-6.2v-6c0-16.8 13.7-30.5 30.5-30.5h18c0.4-0.1 0.9-0.2 1.5-0.3 57.5-11.3 198.5-36 266.3-21 69.6 15.7 175 81.9 207.2 102.9l124.4 33.4c44.4 11.9 70.9 50.8 70.9 103.9v62.5c0 25-20.3 45.3-45.3 45.3h-63.5c-8.6 0-15.6-7-15.6-15.6z,M314.6 616.2c-3.1 0-5.6-2.6-5.6-5.6 0-3.1 2.6-5.6 5.6-5.6h374.8c3.1 0 5.6 2.6 5.6 5.6 0 3.1-2.6 5.6-5.6 5.6H314.6z m575.5-109.1,M299 610.6c0-8.6 7-15.6 15.6-15.6h374.8c8.6 0 15.6 7 15.6 15.6s-7 15.6-15.6 15.6H314.6c-8.6 0-15.6-7-15.6-15.6z m15.6 4.3z,M750.3 658.2c-38.9 0-70.7-31.7-70.7-70.7 0-38.9 31.7-70.7 70.7-70.7 38.9 0 70.7 31.7 70.7 70.7 0 38.9-31.7 70.7-70.7 70.7z m0-130.1c-32.8 0-59.4 26.6-59.4 59.4s26.6 59.4 59.4 59.4 59.4-26.6 59.4-59.4c0-32.7-26.6-59.4-59.4-59.4zM253.7 658.2c-38.9 0-70.7-31.7-70.7-70.7 0-38.9 31.7-70.7 70.7-70.7 38.9 0 70.7 31.7 70.7 70.7 0 38.9-31.8 70.7-70.7 70.7z m0-130.1c-32.8 0-59.4 26.6-59.4 59.4s26.6 59.4 59.4 59.4 59.4-26.6 59.4-59.4c0-32.7-26.6-59.4-59.4-59.4z m0 0,M173 587.5c0-44.5 36.2-80.7 80.7-80.7s80.7 36.2 80.7 80.7-36.2 80.7-80.7 80.7S173 632 173 587.5z m31.3 0c0 27.2 22.2 49.4 49.4 49.4s49.4-22.2 49.4-49.4-22.2-49.4-49.4-49.4-49.4 22.2-49.4 49.4z m465.4 0c0-44.5 36.2-80.7 80.7-80.7s80.7 36.2 80.7 80.7-36.2 80.7-80.7 80.7-80.7-36.2-80.7-80.7z m31.2 0c0 27.2 22.2 49.4 49.4 49.4s49.4-22.2 49.4-49.4-22.2-49.4-49.4-49.4-49.4 22.2-49.4 49.4z,M144.3 393.1h618.6v30H144.3z,M388.222 407.994l1.064-119.7 30 0.267-1.064 119.7z", //箭头图标
        symbolSize: 8, //图标大小
      },
      lineStyle: {
        normal: {
          color: "#fff",
          width: 2, //线条宽度
          opacity: 0.1, //尾迹线条透明度
          curveness: 0, //尾迹线条曲直度
          // type: "dashed",
        },
      },
      // 轨迹数据
      data: [],
    },
    // 汽车  series 4
    {
      type: "lines",
      polyline: true,
      zlevel: 2,
      effect: {
        show: true,
        period: 16, //箭头指向速度，值越小速度越快
        trailLength: 0.3, //特效尾迹长度[0,1]值越大，尾迹越长重
        // 大巴
        symbol:"path://M885.5 269H196.272c-16.898 0-32.556 7.639-42.957 20.959L59.543 410.055C52.1 419.588 48 431.5 48 443.595V590.5c0 30.052 24.448 54.5 54.5 54.5h50.626c2.613 52.275 45.962 94 98.874 94s96.261-41.725 98.874-94h311.251c2.613 52.275 45.962 94 98.874 94s96.261-41.725 98.874-94H885.5c30.052 0 54.5-24.448 54.5-54.5v-267c0-30.051-24.448-54.5-54.5-54.5zM252 709c-38.047 0-69-30.953-69-69s30.953-69 69-69 69 30.953 69 69-30.953 69-69 69z m509 0c-38.047 0-69-30.953-69-69s30.953-69 69-69 69 30.953 69 69-30.953 69-69 69z m149-118.5c0 13.51-10.99 24.5-24.5 24.5h-28.7c-11.105-42.524-49.844-74-95.8-74-45.955 0-84.695 31.476-95.8 74H347.8c-11.105-42.524-49.844-74-95.8-74s-84.695 31.476-95.8 74h-53.7C88.99 615 78 604.01 78 590.5V443.595a24.615 24.615 0 0 1 5.189-15.078l93.771-120.094c4.676-5.988 11.715-9.422 19.312-9.422H885.5c13.51 0 24.5 10.991 24.5 24.5V590.5z,M828 442h-60v-88c0-8.284-6.716-15-15-15s-15 6.716-15 15v88H595v-88c0-8.284-6.716-15-15-15s-15 6.716-15 15v88H423v-88c0-8.284-6.716-15-15-15s-15 6.716-15 15v88H249v-88c0-8.284-6.716-15-15-15s-15 6.716-15 15v88h-59c-8.284 0-15 6.716-15 15s6.716 15 15 15h668c8.284 0 15-6.716 15-15s-6.716-15-15-15z", //箭头图标
        symbolSize: 8, //图标大小
      },
      lineStyle: {
        normal: {
          color: "#fff",
          width: 2, //线条宽度
          opacity: 0.1, //尾迹线条透明度
          curveness: 0, //尾迹线条曲直度
          // type: "dashed",
        },
      },
      // 轨迹数据
      data: [],
    },
  ],
};

$.getJSON(uploadedDataURL, function (geoJson) {
  echarts.registerMap("china", geoJson);
  myChart.hideLoading();

  myChart.setOption(option, true);
});
$("#btn-add").on("click", function () {
  form.submit("route-form", function (data) {
    var field = data.field; // 获取表单全部字段值
    var start_date = new Date(field.date);
    field.date = start_date.getTime() / 1000;
    console.log(field); // 回调函数返回的 data 参数和提交事件中返回的一致
    // 执行提交
    addRoute(field);
    layer.msg(JSON.stringify(field), {
      title: "当前填写的字段值",
    });
  });
  return false;
});
$("#btn-exchange").on("click", function () {
  exchange();
  return false;
});
$("#btn-list").on("click", function () {
  showList();
  return false;
});

// 添加城市
var points = [];
// 获取城市坐标
$.ajax({
  url: "http://localhost/getCity",
  data: "",
  async: true,
  dataType: "json",
  success: function (array) {
    console.log(option.series[0]);
    // cities = "";
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      points.push({ name: array[index][0], value: [array[index][1], array[index][2]], itemStyle: { color: "#f34e2b" } });
    }

    option.series[0].data = points;
    console.log(option.series[0]);
    myChart.setOption(option);
  },
});

let index = -1;
// 添加路径
var plane_route = [];
var train_route = [];
var drive_route = [];
var bus_route = [];
// 获取路径
$.ajax({
  url: "http://localhost/getRoute",
  data: "",
  async: true,
  dataType: "json",
  success: function (array) {
    console.log(array);
    routes = array;
    // cities = "";
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      // 飞机路径
      if (array[index][1] == "plane") {
        plane_route.push({
          coords: [
            [array[index][4], array[index][5]],
            [array[index][6], array[index][7]],
          ],
          lineStyle: { color: "#4ab2e5" },
        });
      }
      // 火车路径
      if (array[index][1] == "train") {
        train_route.push({
          coords: [
            [array[index][4], array[index][5]],
            [array[index][6], array[index][7]],
          ],
          lineStyle: { color: "#b9be23" },
        });
      }
      // 自驾路径
      if (array[index][1] == "drive") {
        drive_route.push({
          coords: [
            [array[index][4], array[index][5]],
            [array[index][6], array[index][7]],
          ],
          lineStyle: { color: "#d4237a" },
        });
      }
      // 路径
      if (array[index][1] == "bus") {
        bus_route.push({
          coords: [
            [array[index][4], array[index][5]],
            [array[index][6], array[index][7]],
          ],
          lineStyle: { color: "#1296db" },
        });
      }
    }
    option.series[1].data = plane_route;
    option.series[2].data = train_route;
    option.series[3].data = drive_route;
    option.series[4].data = bus_route;
    myChart.setOption(option);
  },
});
